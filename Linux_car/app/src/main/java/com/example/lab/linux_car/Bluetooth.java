package com.example.lab.linux_car;

import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewDebug;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Bluetooth extends AppCompatActivity {


    Button On,Off,Visible,list;
    BluetoothAdapter BA;
    Set<BluetoothDevice>pairedDevices;
    ListView lv;
    BluetoothDevice device;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    String fiilliste[] = null;
    byte[] command=new byte[4];
    InputStream mmInStream;
    OutputStream mmOutStrem;

    TextView txtText;
    protected static final int RESULT_SPEECH = 1;
    private static final int REQUEST_ENABLE_BT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        On = (Button)findViewById(R.id.button1);
        Off = (Button)findViewById(R.id.button2);
        Visible = (Button)findViewById(R.id.button3);
        list = (Button)findViewById(R.id.button4);
        lv=(ListView)findViewById(R.id.listView);
        BA = BluetoothAdapter.getDefaultAdapter();
        txtText = (TextView) findViewById(R.id.txtText);

        listViewClick();

        checkBlueToothOpen();

        fab();

    }


    public void listViewClick() {

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("pppp",pairedDevices.toString());
                    for (BluetoothDevice bd : pairedDevices) {
                        Log.e("fffff", bd.toString());
                        Log.e("fffff", Integer.toString(position));
                        Log.e("fffff",fiilliste[position]);

                        //String devicep = lv.getAdapter().getItem(position).toString();
                            if (bd.getName().equals(fiilliste[position])) {
                                //try {
                                    device = bd;

                                    Toast.makeText(getApplicationContext(), "?!" + position, Toast.LENGTH_LONG).show();
                                    //Log.e("device",device.toString());
                                //} catch (Exception ex) {}
                            }
                    }

                try {
                    Log.e("device",device.toString());
                    BluetoothSocket mmSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
                    mmSocket.connect();  // blocking call

                    mmInStream = mmSocket.getInputStream();
                    mmOutStrem = mmSocket.getOutputStream();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    });
    }

    public void checkBlueToothOpen() {
        BA = BluetoothAdapter.getDefaultAdapter();
        // 檢查藍牙是否開啟
        if (!BA.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }

        BA.startDiscovery();
        // 搜尋到藍牙裝置時，呼叫我們的函式
        IntentFilter filter = new IntentFilter( BluetoothDevice.ACTION_FOUND );
        this.registerReceiver( mReceiver, filter );

        // 搜尋的過程結束後，也呼叫我們的函式
        filter = new IntentFilter( BluetoothAdapter.ACTION_DISCOVERY_FINISHED );
        this.registerReceiver(mReceiver, filter);
    }

    public void fab() {

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(
                        RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

                //語言模式和自由形式的語音辨識
                //intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "zh-TW");
                //
                //提示語音開始
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "上吧~超屌阿斯拉");
                //開始執行我們的Intent、語音辨識

                try {

                    startActivityForResult(intent, RESULT_SPEECH);
                    txtText.setText("");
                } catch (ActivityNotFoundException a) {
                    Toast t = Toast.makeText(getApplicationContext(),
                            "Opps! Your device doesn't support Speech to Text",
                            Toast.LENGTH_SHORT);
                    t.show();
                }

                Snackbar.make(view, "哦哦哦哦哦哦哦", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    public void on(View view){
        if (!BA.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 0);
            Toast.makeText(getApplicationContext(),"Turned on"
                    ,Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"Already on",
                    Toast.LENGTH_LONG).show();
        }
    }
    public void off(View view){
        BA.disable();
        Toast.makeText(getApplicationContext(),"Turned off" ,
                Toast.LENGTH_LONG).show();
    }
    public void list(View view){
        pairedDevices = BA.getBondedDevices();

//        List<String> list = new ArrayList<String>();
        ArrayList list = new ArrayList();
        for(BluetoothDevice bt : pairedDevices)
            list.add(bt.getName());

        Toast.makeText(getApplicationContext(),"Showing Paired Devices",
                Toast.LENGTH_SHORT).show();
        final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, list);
        lv.setAdapter(adapter);
    }

    public void visible(View view){
        Intent getVisible = new Intent(BluetoothAdapter.
                ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(getVisible, 0);

    }
    private BroadcastReceiver mReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ArrayList<String> stringarray=new ArrayList<String>();
            ArrayAdapter<String> adapter;

            if (intent.getAction()== BluetoothDevice.ACTION_FOUND)
            {
                pairedDevices= BA.getBondedDevices();
                for (BluetoothDevice bd:pairedDevices)
                {
                    stringarray.add(bd.getName());
                }
            }
            fiilliste=new String[stringarray.size()];
            stringarray.toArray(fiilliste);

            adapter = new ArrayAdapter<String>(Bluetooth.this,android.R.layout.simple_list_item_1, fiilliste);

            lv.setAdapter(adapter);
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    txtText.setText(text.get(0));
                    //Log.e("c",text.get(0));
                    //Toast.makeText(this,"abc",Toast.LENGTH_LONG);

                    Log.e("try","111");
                    this.sendData();
                    Log.e("try","222");


                    //this.onClickBlue();
                }

                break;


            }
        }
    }

    public void sendData() //throws IOException
    {

        String msg= txtText.getText().toString();
        //mmOutStrem.write(1);
        byte[] send=new byte[4];// send = "";
        send [0]= 9;
        send [1]= 9;
        send [2]= 9;
        send [3]= 9;
        if (msg.contains("前")||msg.contains("上")||msg.contains("衝"))
        {
            send[0] = 0;
            Toast.makeText(this,"前",Toast.LENGTH_LONG).show();
        }
        else if (msg.contains("左"))
        {
            send[0] = 1;
            Toast.makeText(this,"左",Toast.LENGTH_LONG).show();
        }
        else if (msg.contains("右"))
        {
            send[0] = 2;
            Toast.makeText(this,"右",Toast.LENGTH_SHORT).show();
        }
        if (mmOutStrem!=null)
        {
            try {
                mmOutStrem.write(send);
                mmOutStrem.flush();
                Toast.makeText(this,"連線成功" + device.getName(),Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        if (mmOutStrem!=null)
//        {
//            try {
//                mmOutStrem.flush();
//                Toast.makeText(this,"連線成功2",Toast.LENGTH_LONG).show();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        else
//            Toast.makeText(this,"連線fail",Toast.LENGTH_LONG).show();
        //Log.e("c",send);
        //Toast.makeText(this,send,Toast.LENGTH_LONG);
    }


}
